package ru.company.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * Аэропорт
 */
@Data
@Entity
@Table(name = "airports")
public class Airport {

    /**
     * Код аэропорта
     */
    @Id
    @Column(name = "airport_code")
    private String airportCode;

    /**
     * Название аэропорта
     */
    @Column(name = "airport_name")
    private String airportName;

    /**
     * Город
     */
    @Column(name = "city")
    private String city;

    /**
     * Координаты аэропорта: долгота
     */
    @Column(name = "longitude")
    private Double longitude;

    /**
     * Координаты аэропорта: широта
     */
    @Column(name = "latitude")
    private Double latitude;

    /**
     * Временная зона аэропорта
     */
    @Column(name = "timezone")
    private String timezone;

    /**
     * Аэропорты отбытия
     */
    @OneToMany(mappedBy = "departureAirport")
    private Set<Flight> departureAirport;

    /**
     * Аэропорты прибытия
     */
    @OneToMany(mappedBy = "arrivalAirport")
    private Set<Flight> arrivalAirport;
}
