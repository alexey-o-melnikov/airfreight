package ru.company.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Бронирование
 */
@Data
@Entity
@Table(name = "bookings")
public class Booking {
    /**
     * Номер бронирования
     */
    @Id
    @Column(name = "book_ref")
    private String bookRef;

    /**
     * Дата бронирования
     */
    @Column(name = "book_date")
    private LocalDateTime bookDate;

    /**
     * Полная сумма бронирования
     */
    @Column(name = "total_amount")
    private Double totalAmount;

    /**
     * Билеты
     */
    @OneToMany(mappedBy = "booking")
    private Set<Ticket> tickets;
}
