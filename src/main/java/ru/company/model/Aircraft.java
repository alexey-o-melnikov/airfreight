package ru.company.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Самолет
 */
@Data
@Entity
@Table(name = "aircrafts")
public class Aircraft {

    /**
     * Код самолета, IATA
     */
    @Column(name = "aircraft_code")
    private String aircraftCode;

    /**
     * Модель самолета
     */
    @Column(name = "model")
    private String model;

    /**
     * Максимальная дальность полета, км
     */
    @Column(name = "range")
    private Integer range;

    /**
     * Рейсы
     */
    @OneToMany(mappedBy = "aircraft")
    private Set<Flight> flights;

    /**
     * Места
     */
    @OneToMany(mappedBy = "aircraft")
    private Set<Seat> seats;
}
