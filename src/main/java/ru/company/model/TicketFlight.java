package ru.company.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Пререлет
 */
@Data
@Entity
@Table(name = "ticket_flights")
@AssociationOverrides({
        @AssociationOverride(name = "pk.ticket", joinColumns = @JoinColumn(name = "ticket_no")),
        @AssociationOverride(name = "pk.flight", joinColumns = @JoinColumn(name = "flight_id"))
})
public class TicketFlight {

    @EmbeddedId
    private TicketFlightId pk = new TicketFlightId();

    /**
     * Класс обслуживания
     */
    @Column(name = "fare_conditions")
    private String fareConditions;

    /**
     * Стоимость перелета
     */
    @Column(name = "amount")
    private Double amount;

    @OneToOne(mappedBy = "ticketFlight")
    private BoardingPass boardingPass;

    @Transient
    public Ticket getTicket() {
        return getPk().getTicket();
    }

    public void setTicket(Ticket ticket) {
        getPk().setTicket(ticket);
    }

    @Transient
    public Flight getFlight() {
        return getPk().getFlight();
    }

    public void setFlight(Flight flight) {
        getPk().setFlight(flight);
    }
}
