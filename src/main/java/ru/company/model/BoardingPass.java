package ru.company.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Посадочный талон
 */
@Data
@Entity
@Table(name = "boarding_passes")
public class BoardingPass {

    @Id
    @AttributeOverrides({
            @AttributeOverride(name = "ticketNo",
                    column = @Column(name = "ticket_no")),
            @AttributeOverride(name = "flightId",
                    column = @Column(name = "flight_id"))
    })
    /**
     * Номер билета
     */
    @Column(name = "ticked_no")
    private String tickedNo;

    /**
     * Идентификатор рейса
     */
    @Column(name = "flight_id")
    private Integer flightId;

    /**
     * Номер посадочного талона
     */
    @Column(name = "boarding_no")
    private Integer boardingNo;

    /**
     * Номер места
     */
    @Column(name = "seat_no")
    private String seatNo;

    @OneToOne
    @PrimaryKeyJoinColumn
    private TicketFlight ticketFlight;
}
