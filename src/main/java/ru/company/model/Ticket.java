package ru.company.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * Билет
 */
@Data
@Entity
@Table(name = "tickets")
public class Ticket {

    /**
     * Номер билета
     */
    @Id
    @Column(name = "ticket_no")
    private String ticketNo;

    /**
     * Идентификатор пассажира
     */
    @Column(name = "passenger_id")
    private String passengerId;

    /**
     * Имя пассажира
     */
    @Column(name = "passenger_name")
    private String passengerName;

    /**
     * Контактные данные пассажира
     */
    @Column(name = "contact_data")
    private String contactData; ///TODO: Переделать маппинг на json

    /**
     * Номер бронирования
     */
    @ManyToOne
    @JoinColumn(name = "book_ref")
    private Booking booking;

    @OneToMany(mappedBy = "pk.ticket")
    private Set<TicketFlight> ticketFlights;
}
