package ru.company.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Место
 */
@Data
@Entity
@Table(name = "seats")
public class Seat {

    /**
     * Код самолета, IATA
     */
    @Id
    @ManyToOne
    @JoinColumn(name = "aircraft_code")
    private Aircraft aircraft_code;

    /**
     * Номер места
     */
    @Id
    @Column(name = "seat_no")
    private String seatNo;

    /**
     * Класс обслуживания
     */
    @Column(name = "fare_conditions")
    private String fareConditions;
}
