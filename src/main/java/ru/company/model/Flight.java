package ru.company.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Рейс
 */
@Data
@Entity
@Table(name = "flights")
public class Flight {

    /**
     * Идентификатор рейса
     */
    @Id
    @Column(name = "flight_id")
    private Integer flightId;

    /**
     * Номер рейса
     */
    @Column(name = "flight_no")
    private String flightNo;

    /**
     * Время вылета по расписанию
     */
    @Column(name = "scheduled_departure")
    private LocalDateTime scheduledDeparture;

    /**
     * Время прилёта по расписанию
     */
    @Column(name = "scheduled_arrival")
    private LocalDateTime scheduledArrival;

    /**
     * Аэропорт отправления
     */
    @ManyToOne
    @JoinColumn(name = "airport_code")
    private Airport departureAirport;

    /**
     * Аэропорт прибытия
     */
    @ManyToOne
    @JoinColumn(name = "airport_code")
    private Airport arrivalAirport;

    /**
     * Статус рейса
     */
    @Column(name = "status")
    private String status;

    /**
     * Код самолета, IATA
     */
    @ManyToOne
    @JoinColumn(name = "aircraft_code")
    private Aircraft aircraft;

    /**
     * Фактическое время вылета
     */
    @Column(name = "actual_departure")
    private LocalDateTime actualDeparture;

    /**
     * Фактическое время прилёта
     */
    @Column(name = "actual_arrival")
    private LocalDateTime actualArrival;

    @OneToMany(mappedBy = "pk.flight")
    private Set<TicketFlight> ticketFlights;
}
