package ru.company.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * Id для TicketFligh
 */
@Data
@Embeddable
public class TicketFlightId {

    /**
     * Номер билета
     */
    @ManyToOne
    private Ticket ticket;

    /**
     * Идентификатор рейса
     */
    @ManyToOne
    private Flight flight;
}
